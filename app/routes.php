<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'ProjectController@listTable');
//Route::get('show/{id}', 'ProjectController@listLog');
Route::get('show/reason/{id}', 'ProjectController@listReasons');
Route::get('reason-data', 'ProjectController@getReasons');

Route::get('project-data', 'ProjectController@getDatatable');
Route::get('budget-data/{project_id}', 'ProjectController@getBudgetData');
Route::get('timeline-data/{project_id}', 'ProjectController@getTimelineData');

Route::get('create', 'ProjectController@createProject');

Route::post('new', 'ProjectController@newProjectPost');
Route::get('new', 'ProjectController@newProjectGet');

Route::post('new/{id}', 'ProjectController@existingProjectPost');
Route::get('new/{id}', 'ProjectController@existingProjectGet');

Route::post('info', 'ProjectController@fillProjectInfoPost');
Route::get('info', 'ProjectController@fillProjectInfoGet');

Route::get('submit/{id}', 'ProjectController@submitProject');
//Route::post('info/{id}', 'ProjectController@editProjectInfoPost');
//Route::get('info/{id}', 'ProjectController@editProjectInfoGet');


Route::post('budget/{id}', 'ProjectController@fillProjectBudgetPost');
Route::get('budget/{id}', 'ProjectController@fillProjectBudgetGet');
Route::get('budget_item/delete/{id}', 'ProjectController@deleteBudgetItem');


//Route::post('edit-budget/{id}', 'ProjectController@editProjectBudgetPost');
//Route::get('edit-budget/{id}', 'ProjectController@editProjectBudgetGet');
//Route::get('edit-budget_item/delete/{id}', 'ProjectController@deleteBudgetItem');

Route::post('timeline/{id}', 'ProjectController@fillProjectTimelinePost');
Route::get('timeline/{id}', 'ProjectController@fillProjectTimelineGet');
Route::get('event/delete/{id}', 'ProjectController@deleteEvent');

Route::get('delete/{id}', 'ProjectController@deleteProject');

Route::get('show/{id}', 'ProjectController@showProject');
//Route::get('show/{id}', 'LogController@listLog');

Route::post('update/{id}', 'ProjectController@updateProject');
Route::get('edit/{id}', 'ProjectController@editProject');

Route::post('update-reason/{id}', 'ProjectController@updateReason');
Route::get('edit-reason/{id}', 'ProjectController@editReason');

Route::post('update-budget/{id}', 'ProjectController@updateBudget');
Route::get('edit-budget/{id}', 'ProjectController@editBudget');



Route::get('show/approve/{id}', 'ProjectController@approve');
Route::get('show/disapprove/{id}', 'ProjectController@disapprove');
Route::get('show/close/{id}', 'ProjectController@close');
// Route::get('show/disapprove/{id}', 'ProjectController@disapprove');

Route::post('disapproving/{id}', 'ProjectController@newReasonPost');
Route::get('disapproving/{id}', 'ProjectController@newReasonGet');
Route::post('reason/update/{id}', 'ProjectController@updateReason');
Route::get('reason/edit/{id}', 'ProjectController@editReason');
Route::get('reason/delete/{id}', 'ProjectController@deleteReason');



/*

Route::get('view/(:num)', function($post) {
 	$post = Post::find($post);
 	return View::make('pages.full')->with('post', $post);
});
Route::get('admin', array('before' => 'auth', 'do' => function() {
	$user = Auth::user();
 	return View::make('pages.new')->with('user', $user);
}));
Route::post('admin', array('before' => 'auth', 'do' => function() {
	 	// let's get the new post from the POST data
	 // this is much safer than using mass assignment
	$new_post = array(
	 'title' => Input::get('title'),
	 'body' => Input::get('body'),
	 'author_id' => Input::get('author_id')
	 );
	 // let's setup some rules for our new data
	 // I'm sure you can come up with better ones
	$rules = array(
	 'title' => 'required|min:3|max:128',
	 'body' => 'required'
	 );
	 // make the validator
	$v = Validator::make($new_post, $rules);
	 if ( $v->fails() )
	 {
	 // redirect back to the form with
	 // errors, input and our currently
	 // logged in user
	 return Redirect::to('admin')
	 ->with('user', Auth::user())
	 ->with_errors($v)
	 ->with_input();
	 }
	 // create the new post
	$post = new Post($new_post);
	$post->save();
	 // redirect to viewing our new post
	 return Redirect::to('view/'.$post->id);
}));
Route::post('login', function() {
	 $userdata = array(
	 'username' => Input::get('username'),
	 'password' => Input::get('password')
	 );
	 if ( Auth::attempt($userdata) )
	 {
	 	return Redirect::to('admin');
	 }
	 else
	 {
	 	return Redirect::to('login')->with('login_errors', true);
	 }
});
Route::get('login', function() {
 	return View::make('pages.login');
});
Route::get('logout', function() {
  	Auth::logout();
 	return Redirect::to('/');
});*/
?>

