<?php

use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function($table) {
		$table->increments('id');
		$table->string('project_name', 128);
		$table->integer('responsible_person_id');
		$table->integer('project_organizer_id');
		$table->date('project_start');
		$table->date('project_end');
		$table->text('description');
		$table->integer('approved_id');
		$table->enum('project_state',array('approved','disapproved', 'created', 'closed', 'running'));
		$table->integer('registration_progress');
		$table->boolean('submited')->default(0);
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
