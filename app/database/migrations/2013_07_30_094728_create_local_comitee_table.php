<?php

use Illuminate\Database\Migrations\Migration;

class CreateLocalComiteeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('local_comitees', function($table) {
		$table->increments('id');
		$table->text('name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('local_comitees');
	}

}
