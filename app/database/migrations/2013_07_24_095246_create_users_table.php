<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

public function up()
{
	Schema::create('users', function($table) {
	$table->increments('id');
	$table->string('username', 128);
	$table->string('nickname', 128);
	$table->string('email', 128);
	$table->string('password', 128);
	$table->integer('role_id');
	$table->integer('local_comitee_id');
	});

}

public function down()
{
    Schema::drop('users');
}

}
