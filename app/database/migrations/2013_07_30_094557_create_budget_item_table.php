<?php

use Illuminate\Database\Migrations\Migration;

class CreateBudgetItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_items', function($table) {
		$table->increments('id');
		$table->string('item_name', 128);
		$table->integer('item_price');
		$table->integer('project_id');
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_items');
	}

}
