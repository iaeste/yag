<?php

use Illuminate\Database\Migrations\Migration;

class CreateTimelineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('timeline', function($table) {
		$table->increments('id');
		$table->string('event_name', 128);
		$table->date('event_start');
		$table->date('event_end');
		$table->integer('project_id');
		$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('timeline');
	}

}
