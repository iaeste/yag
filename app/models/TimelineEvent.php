<?php

class TimelineEvent extends Eloquent
{	
	protected $table = 'timeline';
	protected $fillable = array('*');

	public function project()
	{
		return $this->belongsTo('Project', 'project_id');
	}
}

?>
