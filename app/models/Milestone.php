<?php

class Milestone extends Eloquent
{
	public function project()
	{
		return $this->belongsTo('Project', 'project_id');
	}
}

?>
