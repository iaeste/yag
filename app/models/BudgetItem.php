<?php

class BudgetItem extends Eloquent
{	
	protected $fillable = array('*');

	public function project()
	{
		return $this->belongsTo('Project', 'project_id');
	}
}

?>
