<?php

class Project extends Eloquent {

	 protected $fillable = array('*');
	 
	 public function approved()
	 {
	 	return $this->belongsTo('User','approved_id');
	 }

	 public function responsiblePerson()
	 {			
	 	return $this->belongsTo('User','responsible_person_id');
	 }

	 public function projectOrganizer()
	 {
	 	return $this->belongsTo('User','project_organizer_id');
	 }
}

?>
