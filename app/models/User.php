<?php

class User extends Eloquent {
	 public function role()
	 {
	 	return $this->belongsTo('Role','role_id');
	 }

	 public function localComitee()
	 {
	 	return $this->belongsTo('LocalComitee','local_comitee_id');
	 }
}

?>
