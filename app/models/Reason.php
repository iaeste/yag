<?php

class Reason extends Eloquent
{	
	protected $table = 'reasons';
	protected $fillable = array('*');
	public $timestamps = false;

	public function project()
	{
		return $this->belongsTo('Reason', 'reason_id');
	}
}

?>
