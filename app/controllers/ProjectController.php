<?php

class ProjectController extends BaseController {

    public function listTable()
    {
		$projects = Project::all();
		return View::make('pages.list')->with('projects', $projects);
    }


    public function listReasons($id)
    {
		$reason = Reason::find($id);
		return View::make('pages.showReason')->with('reason', $reason);
    }


    public function getReasons()
    {
    	$reasons=Reason::where('submited', '=', true)->join('projects', 'projects.id', '=', 'reasons.project_id')->select(array('reasons.id',
					  				  'reasons.reason_name'));

    	return Datatables::of($reasons);
    }

    public function getDatatable()
    {
		$projects = Project::where('submited', '=', true)->join('users', 'projects.project_organizer_id', '=', 'users.id')
	   					   ->select(array('projects.id',
					  				  'projects.project_name',
									  'users.nickname',
									  'projects.project_start',
									  'projects.project_end',
									  'projects.project_state',
									  'projects.created_at'));
		return Datatables::of($projects)->add_column('operations','
			<div class="btn-group">
		        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
		            Akce
		            <span class="caret"></span>
		        </button>
		        <ul class="dropdown-menu">
		            <li>
		            	<a href="/index.php/show/{{$id}}">Show</a>
		            </li>
		            <li>
		            	<a href="/index.php/edit/{{$id}}">Edit Project</a>
		            </li>
		             <li>
		            	<a href="/index.php/edit-budget/{{$id}}">Edit Budget</a>
		            </li>
		             <li>
		            	<a href="/index.php/timeline/{{$id}}">Edit Timeline</a>
		            </li>
		            <li>
		            	<a href="/index.php/delete/{{$id}}" onclick="return confirm(\'Are you sure?\')">Delete</a>
		            </li>
		        </ul>
		    </div>
	    ')->make();
    }

	public function newProjectPost()    
	{	
		return Redirect::route('new');
	}

    public function newProjectGet()
    {
    	//$users = User::lists('nickname','id');
 		return View::make('pages.new');//->with('users',$users);
    }

	public function existingProjectPost($id)    
	{	
		return Redirect::route('new'.$id);
	}

    public function existingProjectGet($id)
    {
    	//$users = User::lists('nickname','id');
    	$project = Project::find($id);
		if(TimelineEvent::where('project_id', '=', $id)->count()==0 && $project->registration_progress & 2)
    	if(BudgetItem::where('project_id', '=', $id)->count()==0 )
			$project->registration_progress^=2;
		if(TimelineEvent::where('project_id', '=', $id)->count()==0 && $project->registration_progress & 4)
			$project->registration_progress^=4;
 		return View::make('pages.new')->with('project',$project);//->with('users',$users);
    }
    

    public function fillProjectInfoPost()
    {
    	$new_project = array(

			'projectName'			=>	Input::get('project_name'),
			'responsible'			=>	Input::get('responsible_person_id'),
			'organizer'		 		=>	Input::get('project_organizer_id'),
			'projectStart'			=>	Input::get('project_start'),
			'projectEnd'			=>	Input::get('project_end'),
			'description'			=>	Input::get('description')
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones
		$rules = array(
			'projectName' 	=> 'required|min:3|max:50',
			'responsible'	=> 'required|max:50',
			'organizer'		=> 'required|max:50'
		);
		// make the validator
		$v = Validator::make($new_project, $rules);
		if ( $v->fails() )	{
			return Redirect::to('info')->withErrors($v)->withInput();
		}
		
		// create the new post
		$project = new Project();
		$project->project_name=$new_project['projectName'];
		$project->responsible_person_id=$new_project['responsible'];
		$project->project_organizer_id=$new_project['organizer'];
		$project->project_start=$new_project['projectStart'];
		$project->project_end=$new_project['projectEnd'];
		$project->description=$new_project['description'];
		$project->project_state='created';
		$project->approved_id=1;
		$project->registration_progress=1;
		$project->save();
		return Redirect::to('/new/'.$project->id);

    }

    public function fillProjectInfoGet()
    {
    	$users = User::lists('nickname','id');
 		return View::make('pages.info')->with('users',$users);
    }

 	public function getBudgetData($project_id)
	{
			//where('problem_id', '=', $problem_id)
    	$budget_items = BudgetItem::where('project_id', '=', $project_id)
    				 ->select(array('budget_items.id','budget_items.item_name', 'budget_items.item_price', 'budget_items.created_at', 'budget_items.item_price'));
    	return Datatables::of($budget_items)->add_column('operations', '
    			<a class="btn btn-danger" href="/index.php/budget_item/delete/{{$id}}" onclick="return confirm(\'Are you sure?\');">Delete</a>

    	')->remove_column('id')->make();
    }

    public function deleteBudgetItem($id)
    {
    	$project_id = BudgetItem::find($id)->project_id;
    	BudgetItem::destroy($id);
    	return Redirect::to('/budget/'.$project_id);
    }

    public function fillProjectBudgetPost($id)
    {
    	$new_item = array(

			'itemName'				=>	Input::get('item_name'),
			'itemPrice'				=>	Input::get('item_price'),
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones
		$rules = array(
			'itemName' 	=> 'required|min:3|max:50',
			'itemPrice'	=> 'required|max:10'			
		);
		// make the validator
		$v = Validator::make($new_item, $rules);
		if ( $v->fails() )	{
			return Redirect::to('/budget/'.$id)->withErrors($v)->withInput();
		}
		
		// create the new post
		$item = new BudgetItem();
		$item->item_name=$new_item['itemName'];
		$item->item_price=$new_item['itemPrice'];
		$item->project_id=$id;
		$item->save();

		$project = Project::find($id);
		$project->registration_progress|=2;
		$project->save();

		return Redirect::to('/budget/'.$id);
    }

    public function fillProjectBudgetGet($id)
    {
    	//$users = User::lists('nickname','id');
 		return View::make('pages.budget')->with('project_id',$id);
    }

    public function getTimelineData($project_id)
	{
			//where('problem_id', '=', $problem_id)
    	$timeline = TimelineEvent::where('project_id', '=', $project_id)
    			  ->select(array('timeline.id','timeline.event_name', 'timeline.event_start', 'timeline.event_end'));
    	return Datatables::of($timeline)->add_column('operations', '
    			<a class="btn btn-danger" href="/index.php/event/delete/{{$id}}" onclick="return confirm(\'Are you sure?\');">Delete</a>

    	')->remove_column('id')->make();
    }
    public function deleteEvent($id)
    {
    	$project_id = TimelineEvent::find($id)->project_id;
    	TimelineEvent::destroy($id);
    	return Redirect::to('/timeline/'.$project_id);
    }

    public function fillProjectTimelinePost($id)
    {
    	$new_event = array(

			'event_name'			=>	Input::get('event_name'),
			'event_start'			=>	Input::get('event_start'),
			'event_end'				=>	Input::get('event_end')
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones
		$rules = array(
			'event_name' 	=> 'required|min:3|max:50',
			'event_start'	=> 'required',
			'event_end'		=> 'required'			
		);
		// make the validator
		$v = Validator::make($new_event, $rules);
		if ( $v->fails() )	{
			return Redirect::to('/timeline/'.$id)->withErrors($v)->withInput();
		}
		
		// create the new post
		$event = new TimelineEvent();
		$event->event_name=$new_event['event_name'];
		$event->event_start=$new_event['event_start'];
		$event->event_end=$new_event['event_end'];
		$event->project_id=$id;

		$project = Project::find($id);
		$project->registration_progress|=4;
		$project->save();
		
		$event->save();

		return Redirect::to('/timeline/'.$id);
    }

    public function fillProjectTimelineGet($id)
    {
    	//$users = User::lists('nickname','id');
 		return View::make('pages.timeline')->with('project_id',$id);
    }


    public function deleteProject($id)
    {
    	Project::destroy($id);
    	return Redirect::to('/');
    }

        public function submitProject($id)
    {
    	$project = Project::find($id);
    	$project->submited=true;
    	$project->save();
    	return Redirect::to('/');
    }
/*
    public function editReason($id)
    {
    	$project = Project::find($id);
		$reason = Reason::lists('reason_name','id');
		
 		return View::make('pages.edit-reason')->with('project',$project)->with('reason',$reason);
    }
*/
    public function editProject($id)
    {
    	$project = Project::find($id);
		$users = User::lists('nickname','id');
		
 		return View::make('pages.edit')->with('project',$project)->with('users',$users);
    }

    public function editBudget($id)
    {
    	$project = Project::find($id);
		$users = User::lists('nickname','id');
		
 		return View::make('pages.budget')->with('project',$project)->with('users',$users);
    }
    
     public function updateBudget($id)
    {
    	$new_item = array(

			'itemName'				=>	Input::get('item_name'),
			'itemPrice'				=>	Input::get('item_price'),
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones
		$rules = array(
			'itemName' 	=> 'required|min:3|max:50',
			'itemPrice'	=> 'required|max:10'			
		);
		// make the validator
		$v = Validator::make($new_item, $rules);
		if ( $v->fails() )	{
			return Redirect::to('/edit-budget/'.$id)->withErrors($v)->withInput();
		}
		
		// create the new post
		$item = new BudgetItem();
		$item->item_name=$new_item['itemName'];
		$item->item_price=$new_item['itemPrice'];
		$item->project_id=$id;
		$item->save();

		$project = Project::find($id);
		$project->registration_progress|=2;
		$project->save();

		return Redirect::to(Input::get('back'));
    }


/*
     public function updateReason($id)
    {

    	$new_reason = array(

			'reasonName'			=>	Input::get('reason_name'),

		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones

		$rules = array(
			'reasonName' 	=> 'required|min:3|max:50',
			
		);
		// make the validator
		$v = Validator::make($new_reason, $rules);
		if ( $v->fails() )	{
			return Redirect::to('edit-reason')->withErrors($v)->withInput();
		}
		
		// create the new post
		//$problem = Problem::create($new_problem);
		$reason = Reason::find($id);
		$reason->reason_name=$new_reason['reasonName'];
	
		$project->save();

		return Redirect::to(Input::get('back'));
    }

*/
    public function updateProject($id)
    {

    	$new_project = array(

			'projectName'			=>	Input::get('project_name'),
			'responsible'			=>	Input::get('responsible_person_id'),
			'organizer'		 		=>	Input::get('project_organizer_id'),
			'projectStart'			=>	Input::get('project_start'),
			'projectEnd'			=>	Input::get('project_end'),
			'description'			=>	Input::get('description')
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones

		$rules = array(
			'projectName' 	=> 'required|min:3|max:50',
			'responsible'	=> 'required|max:50',
			'organizer'		=> 'required|max:50'
		);
		// make the validator
		$v = Validator::make($new_project, $rules);
		if ( $v->fails() )	{
			return Redirect::to('edit')->withErrors($v)->withInput();
		}
		
		// create the new post
		//$problem = Problem::create($new_problem);
		$project = Project::find($id);
		$project->project_name=$new_project['projectName'];
		$project->responsible_person_id=$new_project['responsible'];
		$project->project_organizer_id=$new_project['organizer'];
		$project->project_start=$new_project['projectStart'];
		$project->project_end=$new_project['projectEnd'];
		$project->description=$new_project['description'];
		$project->save();

		return Redirect::to(Input::get('back'));
    }

    public function showProject($id)
    {
    	$project = Project::find($id);
    	return View::make('pages.show')->with('project',$project);
    }

    public function approve($id)
    {
    	$project = Project::find($id);
		$project->project_state= "approved";
		$project->save();	
		$user = User::find($project->responsible_person_id);
		$user->save();
	/*	Mail::send('emails.welcome', $data, function($message)
		{
		    $message->to('foo@example.com', 'John Smith')->subject('Welcome!');
		});

		
		Mail::send(array('html.view','text.view'), array('sdasda'), function ($message) {
	    $message->subject('Message Subject');
	    $message->from($user->username, 'Sender Name');
	    $message->to($user->email); // Recipient address
	});

*/
    	return Redirect::to('show/'.$id);
    }

    public function disapprove($id)
    {
    	$project = Project::find($id);
		$project->project_state= "disapproved";
		$project->save();	
		//$reason = new Reason();
		//$reason->text="ASDASDASDASD";
    	return Redirect::to('disapproving/'.$id)->with('project',$project);
    }

    public function close($id)
    {
    	$project = Project::find($id);
		$project->project_state= "closed";
		$project->save();
		$reason=Reason::find($id);	
		$reason->delete($id);

    	return Redirect::to('show/'.$id);
    }


	public function newReasonPost($id)
	{

		$new_reason = array(
			'reason_name' 		=> Input::get('reason_name')

			);

			// let's setup some rules for our new data
			// I'm sure you can come up with better ones
		$rules = array(
			'reason_name' 	=> 'required | min: 3',
			);
			// make the validator
		$v = Validator::make($new_reason, $rules);
		if ( $v->fails() )	{

			return Redirect::to('/disapproving/'.$id)->withErrors($v)->withInput();

		}
		
	
		$reason = new Reason();
		$reason->reason_name=$new_reason['reason_name'];
		$reason->id=$id;
	//	$reason->id=$id;
		$reason->save();

		$project = Project::find($id);
		$project->save();

		return Redirect::to('/show/'.$id);

	}
	public function newReasonGet($id)
	{
		$project = Project::find($id);
		// $reasons = Reason::lists('reason_name','id');
		
 		return View::make('pages.disapproving')->with('project',$project);//->with('reasons',$reasons);

	}

	public function deleteReason($id)
	{
		$project_id = Reason::find($id)->project_id;
		Reason::destroy($id);
		return Redirect::to('/show/'.$project_id);
	}

	public function editReason($id)
	{
		$reason = Reason::find($id);

		return View::make('pages.edit-reason')->with('reason',$reason);
	}

	public function updateReason($id)
	{

		$new_reason = array(

			'reasonName'			=>	Input::get('reason_name'),

		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones

		$rules = array(
			'reasonName' 	=> 'required|min:3|max:50',
			
		);
		// make the validator
		$v = Validator::make($new_reason, $rules);
		if ( $v->fails() )	{
			return Redirect::to('edit-reason')->withErrors($v)->withInput();
		}
		
		// create the new post
		//$problem = Problem::create($new_problem);
		$reason = Reason::find($id);
		$reason->reason_name=$new_reason['reasonName'];
	
		$reason->save();

		return Redirect::to(Input::get('back'));
    
	}

	/*	$new_item = array(

			'itemName'				=>	Input::get('item_name'),
			'itemPrice'				=>	Input::get('item_price'),
		);

		// let's setup some rules for our new data
		// I'm sure you can come up with better ones
		$rules = array(
			'itemName' 	=> 'required|min:3|max:50',
			'itemPrice'	=> 'required|max:10'			
		);
		// make the validator
		$v = Validator::make($new_item, $rules);
		if ( $v->fails() )	{
			return Redirect::to('/budget/'.$id)->withErrors($v)->withInput();
		}
		
		// create the new post
		$item = new BudgetItem();
		$item->item_name=$new_item['itemName'];
		$item->item_price=$new_item['itemPrice'];
		$item->project_id=$id;
		$item->save();

		$project = Project::find($id);
		$project->registration_progress|=2;
		$project->save();

		return Redirect::to('/budget/'.$id);

	*/


}

?>
