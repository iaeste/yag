<!DOCTYPE HTML>
<html lang="en-GB">
<head>
 <meta charset="UTF-8">
 <title>YAG</title>
{{ HTML::style('css/bootstrap.css') }}
{{ HTML::script('js/jquery-1.10.2.js') }}
{{ HTML::script('js/jquery.dataTables.js') }}
{{ HTML::script('js/bootstrap.js') }}
</head>
<body>
	<div class="row-fluid">
		<div class="span6 offset1">
			<h1>YAG</h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span10 offset1">
			@yield('content')
		</div>
	</div>
</body>
</html>
