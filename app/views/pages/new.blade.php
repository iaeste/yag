@extends('templates.layout')
@section('content')

<style>
	.buttons { 
		width:  100px;
		margin-top: 6px;
		margin-left: 30px;
	}
</style>

@if (!(isset($project->registration_progress)))
	<a href="/index.php/info" class="btn btn-large btn-primary buttons">Info</a><br>
	<a href="#" rel="popover" data-content="You have to fill the info first." class="btn btn-large btn-danger buttons">Budget</a><br>
	<a href="#" rel="popover" data-content="You have to fill the info first."  class="btn btn-large btn-danger buttons">Timeline</a>
@endif

@if (isset($project->registration_progress))
	<a href="/index.php/edit/{{$project->id}}" class="btn btn-success btn-large buttons">Info <i class="icon-white icon-ok"></i></a><br>
	@if (!($project->registration_progress & 2))
		<a href="/index.php/budget/{{$project->id}}" class="btn btn-large btn-primary buttons">Budget</a><br>
	@else
		<a href="/index.php/budget/{{$project->id}}" class="btn btn-success btn-large buttons">Budget <i class="icon-white icon-ok"></i></a><br>
	@endif
	@if ($project->registration_progress & 4)
		<a href="/index.php/timeline/{{$project->id}}" class="btn btn-success btn-large buttons">Timeline <i class="icon-white icon-ok"></i></a><br>
	@else
		<a href="/index.php/timeline/{{$project->id}}" class="btn btn-large btn-primary buttons">Timeline</a>
	@endif
	@if ($project->registration_progress == 7)
		<a href="/index.php/submit/{{$project->id}}" class="btn btn-large btn-primary buttons">Submit</a>
	@endif
@endif

<script>
  $("a[rel=popover]").popover();
</script>

@stop
