@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	$('#datatable').dataTable( {
		"sAjaxSource": "/index.php/project-data",
		"aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 7 ] }
       	],
       	"fnCreatedRow": function( nRow, aData, iDataIndex ) {
       		cell = $('td:eq(5)', nRow).html();
      		if ( cell == "approved" )	{		
        		nRow.setAttribute('class','success')
      		}	else if(cell == "disapproved")	{
      			nRow.setAttribute('class','error')
          } else if(cell == "created")  {
            nRow.setAttribute('class','warning')
      		}
          //cell = $('td:eq(0)', nRow).html();
          //alert($('.acceptLink').html());//.attr("href", 'asdsad');
    	}
	} );
} );
</script>
<div class="span11">
	<table class="table table-bordered" id="datatable">
	 <thead><tr>
    <th>ID</th>
    <th>Name</th>
    <th>Organizer</th>
    <th>Start</th>
    <th>End</th>
    <th>State</th>
    <th>Created</th>
    <th width="8%">Action</th>
  </tr></thead>
	</table>
	<a href="/index.php/new" class="btn btn-large btn-primary">New project</a>
</div>
@stop
