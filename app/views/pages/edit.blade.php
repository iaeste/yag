@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'update/'.$project->id, 'method' => 'POST')) }}


<p>{{ Form::label('project_name', 'Name of project') }}</p>
	{{ $errors->first('project_name', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_name', $project->project_name) }}</p>

<p>{{ Form::label('responsible_person_id', 'Responsible person') }}</p>
 	{{ $errors->first('responsible_person_id', '<p class="error">:message</p>') }}
<p>{{ Form::select('responsible_person_id', $users , $project->responsible_person_id) }}</p>
 
<p>{{ Form::label('project_organizer_id', 'Organizer') }}</p>
 	{{ $errors->first('project_organizer_id', '<p class="error">:message</p>') }}
<p>{{ Form::select('project_organizer_id', $users ,$project->project_organizer_id) }}</p>

<p>{{ Form::label('project_start', 'Project starts') }}</p>
 	{{ $errors->first('project_start', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_start', $project->project_start) }}</p>

<p>{{ Form::label('project_end', 'Project ends') }}</p>
 {{ $errors->first('project_end', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_end', $project->project_end) }}</p>

<p>{{ Form::label('description', 'Description') }}</p>
 {{ $errors->first('description', '<p class="error">:message</p>') }}
<p>{{ Form::textarea('description', $project->description) }}</p>
 {{ Form::hidden('back', URL::previous() ) }}	
 <p>{{ Form::submit('Edit') }}</p>
 {{ Form::close() }}
@stop
