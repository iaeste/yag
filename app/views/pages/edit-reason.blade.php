@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'update-reason/'.$reason->id, 'method' => 'POST')) }}


<p>{{ Form::label('reason_name', 'Reason for Disapproval') }}</p>
	{{ $errors->first('reason_name', '<p class="error">:message</p>') }}
<p>{{ Form::text('reason_name', $reason->reason_name) }}</p>
 {{ Form::hidden('back', URL::previous() ) }}	
 <p>{{ Form::submit('Edit') }}</p>
 {{ Form::close() }}
@stop
