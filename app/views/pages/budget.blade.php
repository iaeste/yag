@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	var oTable = $('#datatable').dataTable( {
		"sAjaxSource": "/index.php/budget-data/{{$project_id}}",
		"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0, 1, 3, 4] }],
		"bPaginate": false,
		"bFilter": false,
		"bInfo": false
	} );
	oTable.fnSort( [ [3,'desc'] ] );
} );
</script>

<style type="text/css">
	#wrapper {
	    
	    overflow: hidden; /* will contain if #first is longer tdan #second */
	}
	.wideLabel {
		padding-top: 5px;
		padding-left: 20px;
		padding-right: 12px;
	    float: left; /* add tdis */
	}
	.submitButton {
		/*border: 1px solid black;*/
		padding-left: 20px;
		padding-bottom: 10px;
	}
</style>


{{ Form::open(array('url' => 'budget/'.$project_id, 'method' => 'POST')) }}
<div id="wrapper">
<table>
	<tr>
		<td class="wideLabel">{{ Form::label('item_name', 'Item:') }}</td>
		<td>{{ Form::text('item_name', Input::old('item_name')) }}</td>
		<td class="wideLabel">{{ Form::label('item_price', 'Price:') }}</td>
		<td>{{ Form::text('item_price', Input::old('item_price')) }}</td>
		<td class="submitButton">
			<button type="submit" class="btn btn-primary btn-small"><i class="icon-white icon-plus"></i></button>
		</td>
	</tr>
</table>
</div>
{{ $errors->first('item', '<p class="error">:message</p>') }}
{{ $errors->first('price', '<p class="error">:message</p>') }}

{{ Form::close() }}
<div class="span11">
	<table class="table" id="datatable">
		<thead><tr>
			<th>Item name</th>
			<th width="20%">Price</th>
			<th width="25%">Date added</th>
			<th width="8%">Delete</th>
			<th width="5%">Total Price</th>
		</tr></thead>
	</table>
		<!--<p><b>Total Price: </b> </p> 
	<a href="/index.php/new/{{$project_id}}" class="btn btn-large btn-primary">Done</a>
</div>

@stop