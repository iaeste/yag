@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'update/'.$project->id, 'method' => 'POST')) }}


<p>{{ Form::label('item_name', 'Item:') }}</p>
	{{ $errors->first('item_name', '<p class="error">:message</p>') }}
<p>{{ Form::text('item_name', $project->item_name) }}</p>

<p>{{ Form::label('item_price', 'Price:') }}</p>
 	{{ $errors->first('item_price', '<p class="error">:message</p>') }}
<p>{{ Form::select('item_price', $users , $project->item_price) }}</p>
 

 {{ Form::hidden('back', URL::previous() ) }}	
 <p>{{ Form::submit('Edit') }}</p>
 {{ Form::close() }}
@stop
