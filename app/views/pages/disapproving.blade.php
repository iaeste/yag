@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'disapproving/'.$project->id, 'method' => 'POST')) }}
 
 <p>{{ Form::label('reason_name', 'Reason') }}</p>
 {{ $errors->first('reason_name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('reason_name', Input::old('reason_name')) }}</p>


<button type="submit" class="btn btn-primary"> Submit Reason </button>

 {{ Form::close() }}
@stop
