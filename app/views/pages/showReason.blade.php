@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
  $('#datatable').dataTable( {
    "sAjaxSource": "/index.php/reason-data",
    "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [ 2 ] }
        ],
  } );
} );
</script>
  <h3>Reason of Disapproval: {{$reason->reason_name}}</h3>
  <a href="/index.php/edit-reason/{{$reason->id}}" class="btn btn-large btn-primary">Edit Reason</a>
  <a class="btn btn-danger" href="/index.php/" onclick="return confirm(\'Are you sure?\');">Home</a> 

@stop
