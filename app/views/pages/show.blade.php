@extends('templates.layout')
@section('content')

<div class="span10">
	<h2>{{$project->project_name}}</h2>
	<h3>Responsible: {{$project->responsible_person->nickname}}</h3>
	<h3>Organizer: {{$project->project_organizer->nickname}}</h3>
	<h3>Project Start: {{$project->project_start}}</h3>
	<h3>Project End: {{$project->project_end}}</h3> 
	{{-- <h3>Approved by: {{$project->approved->nickname}}</h3>--}}
	<h3>State: {{$project->project_state}}</h3> 
	<p>Description: {{$project->description}}</p>
	<div style="position: relative; top: 25px;">
		<a href="approve/{{$project->id}}" class="btn btn-large btn-primary">Approve</a>
		@if ($project->project_state != "disapproved")
		<a href="disapprove/{{$project->id}}" class="btn btn-large btn-danger">Disapprove</a>
		@endif
		@if ($project->project_state == "disapproved")
		<a href="reason/{{$project->id}}" class="btn btn-large btn-danger">Show Reason</a>
		@endif

		<a href="close/{{$project->id}}" class="btn btn-large btn-default">Close</a>
	</div>
	<div style="height: 60px;"></div>
</div>
@stop
