@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	var oTable = $('#datatable').dataTable( {
		"sAjaxSource": "/index.php/timeline-data/{{$project_id}}",
		"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 3 ] }],
		"bPaginate": false,
		"bFilter": false,
		"bInfo": false
	} );
	oTable.fnSort( [ [1,'asc'] ] );
} );
</script>

<style type="text/css">
	#wrapper {
	    
	    overflow: hidden; /* will contain if #first is longer tdan #second */
	}
	.wideLabel {
		padding-top: 5px;
		padding-left: 20px;
		padding-right: 12px;
	    float: left; /* add tdis */
	}
	.submitButton {
		/*border: 1px solid black;*/
		padding-left: 20px;
		padding-bottom: 10px;
	}
</style>


{{ Form::open(array('url' => 'timeline/'.$project_id, 'method' => 'POST')) }}
<div id="wrapper">
<table>
	<tr>
		<td class="wideLabel">{{ Form::label('event_name', 'Event:') }}</td>
		<td>{{ Form::text('event_name', Input::old('event_name')) }}</td>
		<td class="wideLabel">{{ Form::label('event_start', 'Start:') }}</td>
		<td>{{ Form::text('event_start', Input::old('event_start')) }}</td>
		<td class="wideLabel">{{ Form::label('event_end', 'End:') }}</td>
		<td>{{ Form::text('event_end', Input::old('event_end')) }}</td>
		<td class="submitButton">
			<button type="submit" class="btn btn-primary btn-small"><i class="icon-white icon-plus"></i></button>
		</td>
	</tr>
</table>
</div>
{{ $errors->first('item', '<p class="error">:message</p>') }}
{{ $errors->first('price', '<p class="error">:message</p>') }}

{{ Form::close() }}
<div class="span11">
	<table class="table" id="datatable">
		<thead><tr>
			<th>Event name</th>
			<th width="20%">Start</th>
			<th width="25%">End</th>
			<th width="8%">Delete</th>
		</tr></thead>
	</table>
	<a href="/index.php/new/{{$project_id}}" class="btn btn-large btn-primary">Done</a>
</div>

@stop