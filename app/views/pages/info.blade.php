@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'info', 'method' => 'POST')) }}
 <!-- title field -->

<p>{{ Form::label('project_name', 'Name of project') }}</p>
	{{ $errors->first('project_name', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_name', Input::old('project_name')) }}</p>

<p>{{ Form::label('responsible_person_id', 'Responsible person') }}</p>
 	{{ $errors->first('responsible_person_id', '<p class="error">:message</p>') }}
<p>{{ Form::select('responsible_person_id', $users , Input::old('responsible_person_id')) }}</p>
 
<p>{{ Form::label('project_organizer_id', 'Organizer') }}</p>
 	{{ $errors->first('project_organizer_id', '<p class="error">:message</p>') }}
<p>{{ Form::select('project_organizer_id', $users ,Input::old('project_organizer_id')) }}</p>

<p>{{ Form::label('project_start', 'Project starts') }}</p>
 	{{ $errors->first('project_start', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_start', "YYYY-MM-DD") }}</p>

<p>{{ Form::label('project_end', 'Project ends') }}</p>
 {{ $errors->first('project_end', '<p class="error">:message</p>') }}
<p>{{ Form::text('project_end', "YYYY-MM-DD") }}</p>

<p>{{ Form::label('description', 'Description') }}</p>
 {{ $errors->first('description', '<p class="error">:message</p>') }}
<p>{{ Form::textarea('description', Input::old('description')) }}</p>

 <p>{{ Form::submit('Create') }}</p>
 {{ Form::close() }}
@stop
